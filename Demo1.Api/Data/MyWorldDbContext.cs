using Demo1.Api.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Demo1.Api.Data;


public class TodoContext : DbContext
{
    public TodoContext(DbContextOptions<TodoContext> options) : base(options)
    {
 
    }
    public DbSet<Todo> Todo { get; set; }
}